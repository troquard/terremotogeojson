def today_delta(n):
    date_format = "%Y-%m-%d"
    import datetime as d
    today = d.datetime.today()
    output_date = today + d.timedelta(days=n)
    return output_date.strftime(date_format)

def yesterday():
    return today_delta(-1)

def tomorrow():
    return today_delta(1)


# string containing the timezone codes of the server configuration
def timezone_codes():
    import time as t
    separator = "/"
    tzn = t.tzname
    timezones = tzn[0]
    for n in tzn[1:]:
        timezones += separator + n
    return timezones


def distance_km(lat1,lon1,lat2,lon2):
    from geopy.distance import vincenty
    return round(vincenty((lat1,lon1), (lat2,lon2)).meters / 1e3, 1)


def country(lat,lon):
    from geopy.geocoders import Nominatim
    geolocator = Nominatim()
    address, _ = geolocator.reverse(str(lat)+","+str(lon), language='en')
    c = address.split(",")
    return c[-1][1:]


def fmt_durations_seconds(s):
    if s >= 2*365*24*3600:
        return str(int(s / (365*24*3600))), 'years'
    if s > 365*24*3600:
        return str(int(s / (365*24*3600))), 'year'
    elif s >= 2*30*24*3600:
        return str(int(s / (30*24*3600))), 'months'
    elif s > 30*24*3600:
        return str(int(s / (30*24*3600))), 'month'
    elif s >= 2*7*24*3600:
        return str(int(s / (7*24*3600))), 'weeks'
    elif s > 7*24*3600:
        return str(int(s / (7*24*3600))), 'week'
    elif s >= 2*24*3600:
        return str(int(s / (24*3600))), 'days'
    elif s > 24*3600:
        return str(int(s / (24*3600))), 'day'
    elif s >= 2*3600:
        return str(int(s / 3600)), 'hours'
    elif s > 3600:
        return str(int(s / 3600)), 'hour'
    elif s >= 2*60:
        return str(int(s / 60)), 'minutes'
    elif s > 60:
        return str(int(s / 60)), 'minute'
    elif s >= 2:
        return str(int(s)), 'seconds'
    elif s < 2:
        return str(int(s)), 'second'
    

# returns a tab of dictionnaries of type
# {'image': mapimage_url, 'pdf':maplink_url}
# maplink_url is possibly None
def gdacs_EQ_maps(LAT,LON,MAXRADIUSKM):
    relevant_maps_urls = []
    import urllib2
    import xml.etree.ElementTree
    gdacs_rss="http://www.gdacs.org/xml/rss.xml"
    namespaces = {'geo': 'http://www.w3.org/2003/01/geo/wgs84_pos#',
                  'gdacs': 'http://www.gdacs.org'}
    response=urllib2.urlopen(gdacs_rss)
    e = xml.etree.ElementTree.parse(response).getroot()
    for c in e.findall('channel'):
        for i in c.findall('item'):
            eventType = i.find('gdacs:eventtype', namespaces).text
            if eventType != "EQ":
                continue
            geopoint = i.find('geo:Point', namespaces)
            latstr = geopoint.find('geo:lat', namespaces).text
            lonstr = geopoint.find('geo:long', namespaces).text
            # if the event is within 3 times the radius MAXRADIUSKM of (LAT,LON)
            if distance_km(float(latstr),str(lonstr),LAT,LON) < MAXRADIUSKM * 3:
                mapimage_url = i.find('gdacs:mapimage', namespaces).text
                if not mapimage_url is None:
                    maplink_url = i.find('gdacs:maplink', namespaces).text
                    relevant_maps_urls.append({'image': mapimage_url,
                                               'pdf': maplink_url})
                resources = i.find('gdacs:resources', namespaces)
                for resource in resources.findall('gdacs:resource', namespaces):
                    if resource.get('id') in ["overviewmap",
                                              "shakemap_populationmap_overview",
                                              "shakemap_populationmap"]:
                        relevant_maps_urls.append({'image': resource.get('url'),
                                                   'pdf': None})
    return relevant_maps_urls
