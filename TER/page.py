import urllib2
import json
from optionsensitiveelements import (OptionSensitiveElements,
                                     BadLocationError,
                                     BadDateError,
                                     BadMagnitudeError,
                                     BadRadiusError,
                                     TimedOut)
from staticelements import StaticElements


def _section_help_options(STRINGS):
    html_code = ""
    html_code += "<h2 class=\"helpoptions\" id=\"options\">"
    html_code += STRINGS['sec_options']
    html_code += "</h2>"
    return html_code

def _section_current_parameters(STRINGS):
    html_code = ""
    html_code += "<h2 class=\"currentparameters\">"
    html_code += STRINGS['sec_current_parameters']
    html_code += "</h2>"
    return html_code

def _section_earthquakes_list(STRINGS):
    html_code = ""
    html_code += "<h2 id=\"eq_list\">"
    html_code += STRINGS['sec_earthquakes']
    html_code += "</h2>"
    return html_code

def _section_maps_earthquakes(STRINGS):
    html_code = ""
    html_code += "<h2 id=\"eq_maps\">"
    html_code += STRINGS['sec_earthquakes_maps']
    html_code += "</h2>"
    return html_code

def _section_maps_emergency(STRINGS):
    html_code = ""
    html_code += "<h2 id=\"emergency_maps\">"
    html_code += STRINGS['sec_emergency_maps_gdacs']
    html_code += "</h2>"
    return html_code



def page(options,
         DEFAULTS, RESOURCES, PARAMS, LINK_URLS,
         API_KEY, OUTPUT_TIME_FORMAT, STRINGS):
    html_code = ""

    static = StaticElements(DEFAULTS,STRINGS,LINK_URLS)
    # Header
    html_code += static.header()

    # Header Links
    html_code += static.header_links()

    option_error=False
    try:
        dynamic = OptionSensitiveElements(options,
                                          DEFAULTS, RESOURCES, PARAMS, STRINGS)
    except BadLocationError as e:
        html_code += "<p class=\"error\">"
        html_code += STRINGS['error_location'].format(str(e)) 
        html_code += "</p>"
        option_error=True
    except BadDateError as e:
        html_code += "<p class=\"error\">"
        html_code += STRINGS['error_date'].format(str(e)) 
        html_code += "</p>"
        option_error=True
    except BadMagnitudeError as e:
        html_code += "<p class=\"error\">"
        html_code += STRINGS['error_magnitude'].format(str(e)) 
        html_code += "</p>"
        option_error=True
    except BadRadiusError as e:
        html_code += "<p class=\"error\">"
        html_code += STRINGS['error_radius'].format(str(e)) 
        html_code += "</p>"
        option_error=True
    except TimedOut as e:
        html_code += "<p class=\"error\">"
        html_code += STRINGS['error_timed_out'].format(str(e)) 
        html_code += "</p>"
        option_error=True
    finally:
        if option_error:
            html_code += _section_help_options(STRINGS)
            html_code += static.help_options()
            html_code += static.footer()
            return html_code
                
            
    # Menu
    html_code += static.menu()

    # Chosen parameters
    html_code += _section_current_parameters(STRINGS)
    html_code += dynamic.current_parameters()
        
    # Earthquakes list
    html_code += _section_earthquakes_list(STRINGS)
    
    request=dynamic.URL+PARAMS[dynamic.SITE]["Xformatgeojson"]
    html_code += "<a href=\""+request+"\">_</a>"
    try:
        response=urllib2.urlopen(request)
    except urllib2.HTTPError:
        html_code += "<p class=\"error\">"
        html_code += (STRINGS['mess_bad_url']
                      .format("<a href=\"#options\">{}</a>")
                      .format(STRINGS['sec_options']))
        html_code += "</p>"

        html_code += _section_help_options(STRINGS)
        html_code += static.help_options()
        html_code += static.footer()
        return html_code
    try:
        data=json.loads(response.read())
    except ValueError:
        html_code += "<p>"
        html_code += STRINGS['mess_no_data']
        html_code += "</p>"

        html_code += _section_maps_emergency(STRINGS)
        html_code += dynamic.maps_emergency()

        html_code += _section_help_options(STRINGS)
        html_code += static.help_options()
        
        html_code += static.footer()
        return html_code

    quakes=data['features']    

    table_code = dynamic.table(quakes, OUTPUT_TIME_FORMAT)
    html_code += table_code

    # Maps
    html_code += _section_maps_earthquakes(STRINGS)
    if "map" in options and options["map"].value == "t":
        if len(quakes) > 0:
            html_code += dynamic.maps_google(API_KEY, quakes)
        else:
            html_code += "<p>"
            html_code += STRINGS['mess_no_data']
            html_code += "</p>"
    else:
        html_code += "<p>"
        html_code += (STRINGS['mess_maps_not_requested']
                      .format("<a href=\"#options\">{}</a>")
                      .format(STRINGS['sec_options']))
        html_code += "</p>"

    # Emergency maps
    html_code += _section_maps_emergency(STRINGS)
    html_code += dynamic.maps_emergency()

    # Help Options
    html_code += _section_help_options(STRINGS)
    html_code += static.help_options()

    # footer
    html_code += static.footer()
    
    return html_code
