RESOURCES = { 'us':
              {'name': "USGS",
               'baseurl': "http://earthquake.usgs.gov/fdsnws/event/1/query?"},
              'it':
              {'name': "INGV",
               'baseurl': "http://webservices.ingv.it/fdsnws/event/1/query?"},
              'fr':
              {'name': "RENASS",
               'baseurl': "http://renass.unistra.fr/fdsnws/event/1/query?"} }

PARAMS = { 'USGS':
           {'Xformatgeojson': "&format=geojson",
            'minmagnitude': "&minmagnitude=",
            'latitude': "&latitude=",
            'longitude': "&longitude=",
            'maxradius': "&maxradiuskm=",
            'Xradiusunit': "km",
            'starttime': "&starttime=",
            'endtime': "&endtime=",
            'Xtimeformat': "nummillisec",
            'Xorderbytime': "&orderby=time",
            'Xurl': "url",
            'Xalert': "alert"},
           'INGV':
           {'Xformatgeojson': "&format=geojson",
            'minmagnitude': "&minmagnitude=",
            'latitude': "&latitude=",
            'longitude': "&longitude=",
            'maxradius': "&maxradiuskm=",
            'Xradiusunit': "km",
            'starttime': "&starttime=",
            'endtime': "&endtime=",
            'Xtimeformat': "%Y-%m-%dT%H:%M:%S.%f",
            'Xorderbytime': "&orderby=time",
            'Xurl': None,
            'Xalert': None },
           'RENASS':
           {'Xformatgeojson': "&format=json",
            'minmagnitude': "&minmagnitude=",
            'latitude': "&latitude=",
            'longitude': "&longitude=",
            'maxradius': "&maxradius=",
            'Xradiusunit': "deg",
            'starttime': "&starttime=",
            'endtime': "&endtime=",
            'Xtimeformat': "%Y-%m-%dT%H:%M:%S.%fZ",
            'Xorderbytime': "&orderby=time",
            'Xurl': "url",
            'Xalert': None }}
