class StaticElements():
    def __init__(self, DEFAULTS, STRINGS, LINK_URLS):
        self.DEFAULTS = DEFAULTS
        self.STRINGS = STRINGS
        self.LINK_URLS = LINK_URLS

        
    def header(self):
        html_code = ""
        html_code += "Content-type: text/html"+"\n"
        html_code += ""+"\n"
        html_code += "<!DOCTYPE html>"
        html_code += "<html><head><meta charset=\"UTF-8\">"
        style = open('styles.css')
        html_code += "<style>"+style.read()+"</style>"
        html_code += "<title>Terremoti</title></head><body>"
        return html_code


    def help_options(self):
        html_code = ""

        html_code += "<p class=\"helpoptions\">"
        str_res = ("<code>TER/app?<b>&s=</b>it</code> {} INGV,"
                   + " <code>TER/app?<b>&s=</b>fr</code> {} RENASS,"
                   + " <code>TER/app?<b>&s=</b>us</code> {} USGS"
                   + " ({}: {})<br>")
        html_code += str_res.format(self.STRINGS['op_to_use_res'],
                                    self.STRINGS['op_to_use_res'],
                                    self.STRINGS['op_to_use_res'],
                                    self.STRINGS['op_by_default'],
                                    self.DEFAULTS['resource'])
        str_mag = "<code>TER/app?<b>&m=</b>5.8</code> {} 5.8 ({}: {}) ({})<br>"
        html_code += str_mag.format(self.STRINGS['op_see_magnitude'],
                                    self.STRINGS['op_by_default'],
                                    str(self.DEFAULTS['minmagnitude']),
                                    self.STRINGS['op_note_resources'])
        str_rad = "<code>TER/app?<b>&r=</b>55.0</code> {} 55.0 km ({}: {})<br>"
        html_code += str_rad.format(self.STRINGS['op_see_radius'],
                                    self.STRINGS['op_by_default'],
                                    str(self.DEFAULTS['maxradiuskm']))
        str_loc = ("<code>TER/app?<b>&l=</b>\"san francisco\"</code> "
                   + "{} San Francisco ({}: {})<br>")
        html_code += str_loc.format(self.STRINGS['op_see_place'],
                                    self.STRINGS['op_by_default'],
                                    self.DEFAULTS['place'])
        str_map = "<code>TER/app?<b>&map=</b>t</code> {} ({}: {})<br>"
        html_code += str_map.format(self.STRINGS['op_see_maps'],
                                    self.STRINGS['op_by_default'],
                                    self.STRINGS['op_def_no_map'])
        str_dates = ("<code>TER/app?"
                     + "<b>&start=</b>2016-10-21"
                     + "<b>&end=</b>2016-10-23</code> {} ({}: {})<br>")
        html_code += str_dates.format(self.STRINGS['op_see_dates'],
                                      self.STRINGS['op_by_default'],
                                      self.STRINGS['op_def_recent'])
        str_combine = ("{}: <code>TER/app?"
                       + "<b>&l=</b>\"san francisco\""
                       + "<b>&m=</b>5.6"
                       + "<b>&map=</b>t</code></p>")
        html_code += str_combine.format(self.STRINGS['op_combine'])
        return html_code


    def header_links(self):
        html_code = ""
        if len(self.LINK_URLS) == 0:
            return html_code
        html_code += "<ul class=\"links\">"
        for i in range(len(self.LINK_URLS)):
            html_code += ("<li><a href=\"" + self.LINK_URLS[i][0]
                          + "\">" + self.LINK_URLS[i][1]+"</a></li>")
            html_code += " "
        html_code += "</ul>"
        return html_code


    def menu(self):
        html_code = ""
        html_code += "<ul class=\"navigation\">"
        eq_list="<li><a href=\"#eq_list\">{}</a></li>"
        html_code += eq_list.format(self.STRINGS['menu_earthquakes'])
        eq_maps="<li><a href=\"#eq_maps\">{}</a></li>"
        html_code += eq_maps.format(self.STRINGS['menu_maps_eartquakes'])
        emergency_maps="<li><a href=\"#emergency_maps\">{}</a></li>"
        html_code += emergency_maps.format(self.STRINGS['menu_maps_emergency'])
        options="<li style=\"float:right\"><a href=\"#options\">{}</a></li>"
        html_code += options.format(self.STRINGS['menu_options'])
        html_code += "</ul>"
        return html_code


    def footer(self):
        html_code =""
        html_code += "</body></html>"
        return html_code
