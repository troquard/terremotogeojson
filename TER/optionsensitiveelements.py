import datetime as d
import conveniencefunctions as cf

MARKER_LABELS="123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"


class ExceptionTemplate(Exception):
    def __call__(self, *args):
        return self.__class__(*(self.args + args))
            
class BadLocationError(ExceptionTemplate):
    pass

class BadDateError(ExceptionTemplate):
    pass

class BadMagnitudeError(ExceptionTemplate):
    pass

class BadRadiusError(ExceptionTemplate):
    pass

class TimedOut(ExceptionTemplate):
    pass



def _get_site(options, DEFAULTS, RESOURCES, PARAMS):
    if "s" in options:
        resource = options["s"].value
    else:
        resource = DEFAULTS['resource']
    return RESOURCES[resource]['name']

def _get_min_magnitude(options, DEFAULTS, RESOURCES, PARAMS):
    if "m" in options:
        try:
            MINMAG=float(options["m"].value)
        except ValueError:
            raise BadMagnitudeError(options["m"].value)
    else:
        MINMAG=DEFAULTS['minmagnitude']
    return MINMAG

def _get_max_radiuskm(options, DEFAULTS, RESOURCES, PARAMS):
    if "r" in options:
        try:
            MAXRADIUSKM=float(options["r"].value)
        except ValueError:
            MAXRADIUSKM=DEFAULTS['maxradiuskm']
            raise BadRadiusError(options["r"].value)
    else:
        MAXRADIUSKM=DEFAULTS['maxradiuskm']
    return MAXRADIUSKM

def _get_location(options, DEFAULTS, RESOURCES, PARAMS):
    if "l" in options:
        from geopy.geocoders import Nominatim
        geolocator = Nominatim()
        try:
            location=geolocator.geocode(options["l"].value)
        except geopy.exc.GeocoderTimedOut:
            raise TimedOut(options["l"].value)
        if location is None:
            raise BadLocationError(options["l"].value)
        address=location.address
        PLACE=address.encode('utf-8', 'replace')
        LAT=location.latitude
        LON=location.longitude
    else:
        PLACE=DEFAULTS['place']
        LAT=DEFAULTS['lat']
        LON=DEFAULTS['lon']
    return PLACE, LAT, LON

def _get_start_end(options, DEFAULTS, RESOURCES, PARAMS):
    if "end" in options and "start" in options:
        start = options["start"].value
        end = options["end"].value
        try:
            d.datetime.strptime(start, '%Y-%m-%d')
        except ValueError:
            raise BadDateError(start)
        try:
            d.datetime.strptime(end, '%Y-%m-%d')
        except ValueError:
            raise BadDateError(end)
        if start > end:
            raise BadDateError(start + " -> " + end)
        else:
            return start, end
    else:
        return cf.yesterday(), cf.tomorrow()

def _get_url(options, DEFAULTS, RESOURCES, PARAMS):
    if "s" in options:
        resource = options["s"].value
    else:
        resource = DEFAULTS['resource']
    URL=RESOURCES[resource]['baseurl']
    start, end = _get_start_end(options, DEFAULTS, RESOURCES, PARAMS)
    URL=URL+"&starttime="+start
    URL=URL+"&endtime="+end
    site=_get_site(options, DEFAULTS, RESOURCES, PARAMS)
    URL=URL+PARAMS[site]["Xorderbytime"]
    _, lat, lon = _get_location(options, DEFAULTS, RESOURCES, PARAMS)
    minmag = _get_min_magnitude(options, DEFAULTS, RESOURCES, PARAMS)
    URL=URL+PARAMS[site]["minmagnitude"]+str(minmag)
    URL=URL+PARAMS[site]["latitude"]+str(lat)
    URL=URL+PARAMS[site]["longitude"]+str(lon)

    if PARAMS[site]["Xradiusunit"] == "deg":
        # 111 km is 1 degree
        maxradiusdegree = _get_max_radiuskm(options,
                                            DEFAULTS, RESOURCES, PARAMS) / 111
        URL=URL+PARAMS[site]["maxradius"]+str(maxradiusdegree)
    else: # we suppose radius is expressed in km
        URL=(URL
             + PARAMS[site]["maxradius"]
             + str(_get_max_radiuskm(options, DEFAULTS, RESOURCES, PARAMS)))

    return URL



class OptionSensitiveElements:
    def __init__(self, options, DEFAULTS, RESOURCES, PARAMS, STRINGS):
        self.SITE = _get_site(options, DEFAULTS, RESOURCES, PARAMS)
        self.PLACE,self.LAT,self.LON = _get_location(options,
                                                     DEFAULTS, RESOURCES, PARAMS)
        self.MINMAG = _get_min_magnitude(options, DEFAULTS, RESOURCES, PARAMS)
        self.MAXRADIUSKM = _get_max_radiuskm(options,
                                             DEFAULTS, RESOURCES, PARAMS)
        self.START, self.END = _get_start_end(options,
                                              DEFAULTS, RESOURCES, PARAMS)
        self.URL = _get_url(options, DEFAULTS, RESOURCES, PARAMS)
        self.DEFAULTS = DEFAULTS
        self.PARAMS = PARAMS
        self.STRINGS = STRINGS

            
    def current_parameters(self):
        html_code = ""
        html_code += ("<p class=\"currentparameters\"><b>{}</b>"
                      .format(self.STRINGS['cp_showing_earthquakes']))
        html_code += (" ({})<br>"
                      .format(self.STRINGS['cp_using_data'])
                      .format(self.SITE))
        html_code += ("{}<br>"
                      .format(self.STRINGS['cp_magnitude_over'])
                      .format(str(self.MINMAG)))
        html_code += ("{}<br>"
                      .format(self.STRINGS['cp_occur_between'])
                      .format(self.START, self.END))
        html_code += ("{}<br>"
                      .format(self.STRINGS['cp_radius'])
                      .format(str(str(self.MAXRADIUSKM))))
        html_code += ("{}<br>"
                      .format(self.STRINGS['cp_from'])
                      .format(self.PLACE))
        html_code += (self.STRINGS['cp_change']
                      .format("<a href=\"#options\">{}</a>")
                      .format(self.STRINGS['sec_options']))
        html_code += "</p>"
        return html_code

    
    def alert_color(self, quake):
        if self.PARAMS[self.SITE]['Xalert'] != None:
            alert_color=(quake
                         .get('properties')
                         .get(self.PARAMS[self.SITE]['Xalert']))
        else:
            magnitude=quake.get('properties').get('mag')
            if magnitude < 5:
                alert_color="blue"
            elif magnitude >= 5 and magnitude < 5.5:
                alert_color="yellow"
            elif magnitude >= 5.5 and magnitude < 6:
                alert_color="orange"
            else:
                alert_color="red"
        return alert_color

    
    def table(self, quakes, OUTPUT_TIME_FORMAT):
        # we group the quakes in "sections"; we use these control flags
        recent_done = False
        hour_done = False
        day_done = False
        old_done = False
        place_short = ((self.PLACE).split(","))[0]
        html_code = "<table><tr><th></th>"
        html_code += "<th>{}</th>".format(self.STRINGS['table_how_long_ago'])
        html_code += "<th>{}</th>".format(self.STRINGS['table_magnitude'])
        html_code += ("<th>{} {}</th>"
                      .format(self.STRINGS['table_distance_from'],
                              place_short))
        html_code += ("<th>{} ({})</th>"
                      .format(self.STRINGS['table_when'],
                              cf.timezone_codes()))
        html_code += "<th>{}</th></tr>".format(self.STRINGS['table_where'])

        for i in range(len(quakes)):
            # magnitude
            magnitude=quakes[i].get('properties').get('mag')
            # where
            place=quakes[i].get('properties').get('place').encode('utf-8', 'replace')
            lat=quakes[i].get('geometry').get('coordinates')[1]
            lon=quakes[i].get('geometry').get('coordinates')[0]
            # distance
            distance=str(cf.distance_km(self.LAT,self.LON,lat,lon))
            # when
            if self.PARAMS[self.SITE]["Xtimeformat"] == "nummillisec":
                timestamp=quakes[i].get('properties').get('time') / 1e3
            else:
                timestamp=(d.datetime.strptime(
                    quakes[i].get('properties').get('time')
                    , self.PARAMS[self.SITE]["Xtimeformat"])
                           - d.datetime(1970,1,1)).total_seconds()
            timestr=(d.datetime
                     .fromtimestamp(timestamp)
                     .strftime(OUTPUT_TIME_FORMAT))
            # quake url if exists
            if self.PARAMS[self.SITE]['Xurl'] != None:
                quake_url=(quakes[i]
                           .get('properties')
                           .get(self.PARAMS[self.SITE]['Xurl'])).encode('utf-8', 'replace')
            # GROUPING / SECTION HEADER ROW
            seconds_ago=(d.datetime.now()
                         - d.datetime.fromtimestamp(timestamp)).total_seconds()
            row_group = ("<tr><th></th><th><b>{}</b></th>"
                         + "<th></th><th></th><th></th><th></th></tr>")
            if seconds_ago < 1000 and not recent_done:
                html_code += row_group.format(self.STRINGS['table_sec_recent'])
                recent_done = True
            elif seconds_ago >= 1000 and seconds_ago < 3600 and not hour_done:
                html_code += row_group.format(self.STRINGS['table_sec_hour'])
                hour_done = True
            elif seconds_ago >= 3600 and seconds_ago < 3600*24 and not day_done:
                html_code += row_group.format(self.STRINGS['table_sec_day'])
                day_done = True
            elif (seconds_ago >= 3600*24 and not old_done):
                html_code += row_group.format(self.STRINGS['table_sec_old'])
                old_done = True
            # START ROW DATA
            # color
            alert_color = self.alert_color(quakes[i])
            if alert_color == "red":
                html_code += "<tr class=\"alertred\">"
            elif alert_color == "orange":
                html_code += "<tr class=\"alertorange\">"
            elif alert_color == "yellow":
                html_code += "<tr class=\"alertyellow\">"
            else:
                html_code += "<tr>"
            # marker: application quake id. Matches markers on the google maps.
            if i < len(MARKER_LABELS):
                if self.PARAMS[self.SITE]['Xurl'] != None:
                    html_code += ("<td><a href=\""+quake_url+"\">"
                                  + MARKER_LABELS[i]
                                  + "</a></td>")
                else:
                    html_code += "<td>"+MARKER_LABELS[i]+"</td>"
            else:
                if self.PARAMS[self.SITE]['Xurl'] != None:
                    html_code += "<td><a href=\""+quake_url+"\">www</a></td>"
                else:
                    html_code += "<td></td>"
            # other data
            str_ago = cf.fmt_durations_seconds(seconds_ago)
            html_code += ("<td>{}</td>"
                          .format(self.STRINGS['ago'])
                          .format(str_ago[0] + " " + self.STRINGS[str_ago[1]]))
            html_code += "<td>"+str(magnitude)+"</td>"
            html_code += "<td>"+distance+" km"+"</td>"
            html_code += "<td>"+timestr+"</td>"
            html_code += "<td>"+place+"</td>"
            html_code += "</tr>"
            # END ROW DATA
        html_code += "</table>"    

        return html_code

    
    def markers(self, quakes):
        markers=""
        for i in range(min(len(quakes),len(MARKER_LABELS))):
            marker=""
            lat=quakes[i].get('geometry').get('coordinates')[1]
            lon=quakes[i].get('geometry').get('coordinates')[0]

            alert_color = self.alert_color(quakes[i])
            marker=marker+"&markers=color:"
            if alert_color == "red":
                marker=marker+"red"
            elif alert_color == "orange":
                marker=marker+"orange"
            elif alert_color == "yellow":
                marker=marker+"yellow"
            else:
                marker=marker+"blue"
            marker=(marker+"%7Clabel:"+MARKER_LABELS[i]
                    +"%7C"+str(lat)+","+str(lon))
            markers=markers+marker

        return markers

        
    def maps_google(self, API_KEY, quakes):
        html_code = ""
        markers = self.markers(quakes)
        base_map_url = "https://maps.googleapis.com/maps/api/staticmap"
        global_map_url=(base_map_url + "?center="
                        + str(self.LAT)+","+str(self.LON)
                        + "&zoom=9&size=640x640&maptype=roadmap"
                        + markers+"&key="+API_KEY)
        html_code += "<img src=\""+global_map_url+"\" alt=\"global map\">"
        last_event_lat=quakes[0].get('geometry').get('coordinates')[1]
        last_event_lon=quakes[0].get('geometry').get('coordinates')[0]
        last_event_map_url=(base_map_url + "?center="
                            + str(last_event_lat)+","+str(last_event_lon)
                            + "&zoom=12&size=640x640&maptype=terrain"
                            + markers+"&key="+API_KEY)
        html_code += "<br>"
        html_code += ("<img src=\"" + last_event_map_url
                      + "\" alt=\"last event map\">")

        return html_code


    def maps_emergency(self):
        html_code = ""
        maps_urls = cf.gdacs_EQ_maps(self.LAT,self.LON,self.MAXRADIUSKM)
        if len(maps_urls) > 0:
            for mu in maps_urls:
                if mu["pdf"] is None:
                    html_code += ("<img src=\""+mu["image"]
                                  + "\" alt=\"emergency map "
                                  + mu["image"]+"\"><br>")
                else:
                    html_code += ("<a href=\""+mu["pdf"]
                                  + "\"><img src=\""
                                  + mu["image"]
                                  + "\" width=\"640\" alt=\"emergency map "
                                  + mu["image"]+"\"></a><br>")
        else:
            html_code += ("<p>{}</p>"
                          .format(self.STRINGS['mess_no_emergency_map']))
        return html_code
