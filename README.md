# One website for many earthquake resources #

Some institutes around the world allow one to query their database and get a list of eartquake in geojson format. This application is a middleware to query the resources of the institutes USGS https://www.usgs.gov/, INGV http://www.ingv.it/ and RENASS http://renass.unistra.fr/.

Moreover, if there is a relevant disaster alert by the GDACS (Global Disaster Alert and Coordination System) the website will pull the maps from http://www.gdacs.org/.

# How it looks like #

A screenshot of the website: https://bitbucket.org/troquard/terremotogeojson/src/master/screenshot.jpeg.

# Options #

TER/app?&s=it to use the resources INGV, TER/app?&s=fr to use the resources RENASS, TER/app?&s=us to use the resources USGS (by default: us)

TER/app?&m=5.8 to see the earthquakes of magnitude over 5.8 (by default: 4.5) (note: some resources do not show earthquakes of low magnitude in some countries)

TER/app?&r=55.0 to see the earthquakes in a radius of 55.0 km (by default: 100.0)

TER/app?&l="san francisco" to see the earthquakes around San Francisco (by default: Colledara)

TER/app?&map=t to see the earthquakes on the map (by default: no map)

TER/app?&start=2016-10-21&end=2016-10-23 to see the earthquakes between two dates (by default: earthquakes occuring recently)

Combine these options without any particular order: TER/app?&l="san francisco"&m=5.6&map=t

# Setup #

Place the TER/ directory in /usr/lib/cgi-bin/.

The option showing maps uses the Google Static Map API . See https://developers.google.com/maps/documentation/static-maps/ and https://developers.google.com/maps/documentation/static-maps/intro. It requires identification and so a key must be provided. A key can be obtained here https://developers.google.com/maps/documentation/static-maps/get-api-key. Then, in the file app, replace API_KEY="Your API key here" with your API key.

By editing the file app, you can also change the language and the default behaviours.